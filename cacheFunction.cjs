function cacheFunction(cb)
{
    
    let cache={};
    if(typeof cb==='function')
    {
        return function innerCachesFun(...argument)
        {
            let getArgs=JSON.stringify(argument);
            if(getArgs in cache)
            {
                return cache[getArgs];
            }
            else{
                let getResult=cb(...argument);
                cache[getArgs]=getResult;
                return getResult;
            }

        }
    }
    else{
        throw new Error('Invalid Data');
    }
}

module.exports=cacheFunction;