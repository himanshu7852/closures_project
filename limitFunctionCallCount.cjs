function limitFunctionCallCount(cb, n){
    let setFunctionLimit=0;
    if(typeof cb==='function'&& typeof n==='number')
    {
        return function resultLimitFun(...passedParameter){

            if(setFunctionLimit>=n)
            {
                return null;
            }
            else{
                setFunctionLimit++;
                return cb(...passedParameter);
            }
        }
    }
    else{
        throw new Error('Invalid Data');
    }
    
}

module.exports=limitFunctionCallCount;